// -----------------------------------------------------------------------------
// File: main.cc
// -----------------------------------------------------------------------------
//
// gRPC client.

#include <chrono>
#include <thread>

#include "google/protobuf/repeated_field.h"
#include "grpcpp/grpcpp.h"
#include "protos/endpoints.grpc.pb.h"

namespace regnum {

using google::protobuf::RepeatedPtrField;
using grpc::Channel;
using grpc::ClientContext;
using grpc::Status;

class EndpointsClient {
 public:
  EndpointsClient(std::shared_ptr<Channel> channel)
      : stub_(proto::Endpoints::NewStub(channel)) {}

  // Assembles the client's payload, sends it and presents the response back
  // from the server.
  RepeatedPtrField<std::string> GetEndpoints() {
    proto::GetEndpointsRequest request;
    proto::GetEndpointsResponse response;

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->GetEndpoints(&context, request, &response);

    // Act upon its status.
    if (status.ok()) {
      return response.endpoint_id();
    } else {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
      return RepeatedPtrField<std::string>();
    }
  }

  proto::GetResponse Get(std::string id) {
    proto::GetRequest request;
    proto::GetResponse response;

    request.set_endpoint_id(id);

    // Context for the client. It could be used to convey extra information to
    // the server and/or tweak certain RPC behaviors.
    ClientContext context;

    // The actual RPC.
    Status status = stub_->Get(&context, request, &response);

    const google::protobuf::EnumDescriptor *type_descriptor =
        proto::Type_descriptor();
    const google::protobuf::EnumDescriptor *state_descriptor =
        proto::State_descriptor();
    std::string type_str =
        type_descriptor->FindValueByNumber(response.type())->name();
    std::string state_str =
        state_descriptor->FindValueByNumber(response.state())->name();

    std::cout << "name     " << response.name() << std::endl;
    std::cout << "is_muted " << response.is_muted() << std::endl;
    std::cout << "volume   " << response.volume() << std::endl;
    std::cout << "state    " << state_str << std::endl;
    std::cout << "type     " << type_str << std::endl;

    // Act upon its status.
    if (!status.ok()) {
      std::cout << status.error_code() << ": " << status.error_message()
                << std::endl;
    }
    return response;
  }

 private:
  std::unique_ptr<proto::Endpoints::Stub> stub_;
};

}  // namespace regnum

int main(int argc, char **argv) {
  // Instantiate the client. It requires a channel, out of which the actual RPCs
  // are created. This channel models a connection to an endpoint specified by
  // the argument "--target=" which is the only expected argument.
  // We indicate that the channel isn't authenticated (use of
  // InsecureChannelCredentials()).
  std::string target_str;
  std::string arg_str("--target");
  if (argc > 1) {
    std::string arg_val = argv[1];
    size_t start_pos = arg_val.find(arg_str);
    if (start_pos != std::string::npos) {
      start_pos += arg_str.size();
      if (arg_val[start_pos] == '=') {
        target_str = arg_val.substr(start_pos + 1);
      } else {
        std::cout << "The only correct argument syntax is --target="
                  << std::endl;
        return 0;
      }
    } else {
      std::cout << "The only acceptable argument is --target=" << std::endl;
      return 0;
    }
  } else {
    target_str = "localhost:50051";
  }
  regnum::EndpointsClient endpoints_client(
      grpc::CreateChannel(target_str, grpc::InsecureChannelCredentials()));
  google::protobuf::RepeatedPtrField<std::string> response =
      endpoints_client.GetEndpoints();
  while (true) {
    for (std::string endpoint_id : response) {
      std::cout << "Endpoints received: " << endpoint_id << std::endl;
      endpoints_client.Get(endpoint_id);
      // std::cout << "Name is: " << endpoints_client.Get(endpoint_id) <<
      // std::endl;
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
  }

  return 0;
}
