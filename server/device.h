// -----------------------------------------------------------------------------
// File: device.h
// -----------------------------------------------------------------------------
//
// Device manipulation functions.

#ifndef DEVICE_H_
#define DEVICE_H_

#include <mmdeviceapi.h>

#include "windows-inc.h"

namespace regnum {

/** Gets a mmDeviceEnumerator. */
CComPtr<IMMDeviceEnumerator> GetDeviceEnumerator();

}  // namespace regnum

#endif  // DEVICE_H_
