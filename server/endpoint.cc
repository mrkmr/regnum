// -----------------------------------------------------------------------------
// File: endpoint.cc
// -----------------------------------------------------------------------------

#include "endpoint.h"

#include "util.h"

namespace regnum {

Endpoint::Endpoint(CComPtr<IMMDevice> _device) : device_(_device) {
  HRESULT hr;

  // Get IMMEndpoint - for EDataFlow type (render or capture)
  endpoint_ = device_;
  if (!endpoint_) {
    LOG(FATAL) << "Failed to get endpoint of device." << endl;
  }

  // Get IAudioEndpointVolume - for audio volume/mute
  hr =
      device_->Activate(__uuidof(IAudioEndpointVolume), CLSCTX_INPROC_SERVER,
                        NULL, reinterpret_cast<void**>(&(this->endpoint_vol_)));
  if (hr != S_OK) {
    LOG(FATAL) << "Failed to get audio endpoint volume of device." << endl;
    throw std::runtime_error("Can't proceed.");
  }

  // Initial caching.
  id_ = GetIdUncached();
  name_ = GetNameUncached();
  EndpointState state_ = EndpointState::Unknown;

  endpoint_volume_ = std::make_shared<EndpointVolume>();
  endpoint_volume_->SetMute(GetMuteUncached());
  endpoint_volume_->SetVolume(GetVolumeUncached());

  // Callbacks to see volume and mute changes.
  volume_notification_client_ = new VolumeNotificationClient(endpoint_volume_);
  hr = endpoint_vol_->RegisterControlChangeNotify(volume_notification_client_);
  if (hr != S_OK) {
    util::Fatal("Failed to register (volume) control change notify.", hr);
  }
}

const std::string Endpoint::GetId() const { return id_; }

const std::string Endpoint::GetIdUncached() const {
  HRESULT hr;
  LPWSTR wstrId;

  hr = device_->GetId(&wstrId);
  if (hr != S_OK) {
    util::Fatal("Failed to get unique device ID.", hr);
  }

  return util::WideStringToString(wstrId);
}

const std::string Endpoint::GetName() const { return name_; }

const std::string Endpoint::GetNameUncached() const {
  HRESULT hr;
  IPropertyStore* properties;
  PROPVARIANT pv;

  // Open property store.
  hr = device_->OpenPropertyStore(STGM_READ, &properties);
  if (hr != S_OK) {
    LOG(ERROR) << "Failed to get device properties for device "
               << "<stub>." << endl;
  }

  // Get device name from property store.
  hr = properties->GetValue(PKEY_Device_FriendlyName, &pv);
  if (hr != S_OK) {
    LOG(ERROR) << "Failed to get friendly name for device "
               << "<stub>." << endl;
  }

  return util::WideStringToString(pv.pwszVal);
}

// TODO: bool Endpoint::IsDefault();

bool Endpoint::GetMute() { return endpoint_volume_->GetMute(); }

bool Endpoint::GetMuteUncached() const {
  BOOL bMute;
  HRESULT hr;

  hr = this->endpoint_vol_->GetMute(&bMute);
  if (hr != S_OK) {
    LOG(ERROR) << "Failed to get mute state." << endl;
  }

  return (bMute ? true : false);
}

float Endpoint::GetVolume() { return endpoint_volume_->GetVolume(); }

float Endpoint::GetVolumeUncached() const {
  HRESULT hr;
  float fLevel;

  hr = this->endpoint_vol_->GetMasterVolumeLevelScalar(&fLevel);
  if (hr != S_OK) {
    LOG(ERROR) << "Failed to get master volume level." << endl;
  }

  return fLevel;
}

EndpointState Endpoint::GetState() const { return state_; }

EndpointState Endpoint::GetStateUncached() const {
  DWORD dwState;
  HRESULT hr;

  hr = device_->GetState(&dwState);

  if (hr != S_OK) {
    LOG(ERROR) << "Failed to get state for device." << endl;
    return EndpointState::ErrorGettingState;
  }

  switch (dwState) {
    case DEVICE_STATE_ACTIVE:
      return EndpointState::Active;
    case DEVICE_STATE_DISABLED:
      return EndpointState::Disabled;
    case DEVICE_STATE_NOTPRESENT:
      return EndpointState::NotPresent;
    case DEVICE_STATE_UNPLUGGED:
      return EndpointState::Unplugged;
    default:
      return EndpointState::Unknown;
  }
}

EndpointType Endpoint::GetType() const { return type_; }

EndpointType Endpoint::GetTypeUncached() const {
  EDataFlow dataFlow;
  HRESULT hr;

  hr = endpoint_->GetDataFlow(&dataFlow);

  if (hr != S_OK) {
    LOG(ERROR) << "Failed to get data flow for device." << endl;
    return EndpointType::ErrorGettingType;
  }

  switch (dataFlow) {
    case eRender:
      return EndpointType::Render;
    case eCapture:
      return EndpointType::Capture;
    case eAll:
      return EndpointType::All;
    default:
      return EndpointType::Unknown;
  }
}

}  // namespace regnum
