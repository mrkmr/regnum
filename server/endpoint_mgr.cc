// -----------------------------------------------------------------------------
// File: endpoint_mgr.cc
// -----------------------------------------------------------------------------

#include "endpoint_mgr.h"

#include "device.h"
#include "notification_clients.h"
#include "util.h"

namespace regnum {

EndpointMgr::EndpointMgr() {
  HRESULT hr;
  UINT uNumEndpoints;

  CComPtr<IMMDeviceCollection> endpoints;
  CComPtr<IMMDeviceEnumerator> device_enumerator = GetDeviceEnumerator();

  LOG(INFO) << "Getting all audio devices." << endl;
  hr = GetDeviceEnumerator()->EnumAudioEndpoints(eAll, DEVICE_STATE_ACTIVE,
                                                 &endpoints);
  if (hr != S_OK) {
    util::Fatal("Failed to get all audio endpoints.", hr);
  }
  LOG(INFO) << "Got all audio endpoints." << endl;

  hr = endpoints->GetCount(&uNumEndpoints);
  if (hr != S_OK) {
    util::Fatal("Failed to get count of audio endpoints.", hr);
  }
  LOG(INFO) << "Got count of audio endpoints: " << uNumEndpoints << "." << endl;

  // Add all endpoints to the endpoint set.
  for (UINT i = 0; i < uNumEndpoints; ++i) {
    CComPtr<IMMDevice> pDevice;

    hr = endpoints->Item(i, &pDevice);
    if (hr != S_OK) {
      LOG(ERROR) << "Failed to get device " << i << "." << endl;
    }

    std::shared_ptr<Endpoint> endp = std::make_shared<Endpoint>(pDevice);
    std::string id = endp->GetId();
    LOG(INFO) << "ID: " << id << "." << endl;
    endpoints_[id] = endp;
    // endpoints_.emplace({id, endp});
  }

  endpoint_notification_client_ = new EndpointNotificationClient();
  hr = device_enumerator->RegisterEndpointNotificationCallback(
      endpoint_notification_client_);
  if (hr != S_OK) {
    util::Fatal("Failed to register endpoint notification callback.", hr);
  }
}

const std::map<std::string, std::shared_ptr<Endpoint>>
EndpointMgr::GetEndpoints() const {
  return this->endpoints_;
}

}  // namespace regnum
