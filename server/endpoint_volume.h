// -----------------------------------------------------------------------------
// File: endpoint_volume.h
// -----------------------------------------------------------------------------
//
// Endpoint levels like volume and mute.

#ifndef ENDPOINT_VOLUME_H
#define ENDPOINT_VOLUME_H

#include <mutex>

namespace regnum {

/** Thread safe. */
class EndpointVolume {
 private:
  bool mute_ = false;
  float volume_ = 0.0;

  std::mutex mutex_;

 public:
  bool GetMute();
  float GetVolume();

  void SetMute(bool mute);
  void SetVolume(float volume);
};

}  // namespace regnum

#endif  // ENDPOINT_VOLUME_H
