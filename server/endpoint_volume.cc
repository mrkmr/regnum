// -----------------------------------------------------------------------------
// File: endpoint_volume.cc
// -----------------------------------------------------------------------------

#include "endpoint_volume.h"

namespace regnum {

bool EndpointVolume::GetMute() {
  std::lock_guard<std::mutex> guard(mutex_);
  return mute_;
}

float EndpointVolume::GetVolume() {
  std::lock_guard<std::mutex> guard(mutex_);
  return volume_;
}

void EndpointVolume::SetMute(bool mute) {
  std::lock_guard<std::mutex> guard(mutex_);
  mute_ = mute;
}

void EndpointVolume::SetVolume(float volume) {
  std::lock_guard<std::mutex> guard(mutex_);
  volume_ = volume;
}

}  // namespace regnum
