// -----------------------------------------------------------------------------
// File: device.cc
// -----------------------------------------------------------------------------

#include "device.h"

#include <mutex>

using std::lock_guard;
using std::mutex;

#include "util.h"

namespace regnum {

CComPtr<IMMDeviceEnumerator> GetDeviceEnumerator() {
  CComPtr<IMMDeviceEnumerator> device_enumerator;

  HRESULT res = CoCreateInstance(
      __uuidof(MMDeviceEnumerator), 0, CLSCTX_INPROC_SERVER,
      __uuidof(IMMDeviceEnumerator), (void**)&device_enumerator);
  if (res != S_OK) {
    util::Fatal("Failed to CoCreateInstance()", res);
  }

  return device_enumerator;
}

}  // namespace regnum
