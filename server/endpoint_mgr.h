// -----------------------------------------------------------------------------
// File: endpoint_mgr.h
// -----------------------------------------------------------------------------
//
// Manages all the device endpoints.

#ifndef ENDPOINT_MGR_H_
#define ENDPOINT_MGR_H_

#include <map>

#include "endpoint.h"
#include "notification_clients.h"
#include "windows-inc.h"

namespace regnum {

/**
 * Manages all the device endpoints.
 * Uses singleton pattern.
 */
class EndpointMgr {
 private:
  std::map<std::string, std::shared_ptr<Endpoint>> endpoints_;

  CComPtr<EndpointNotificationClient> endpoint_notification_client_;

  /**
   * Constructor for EndpointMgr.
   */
  EndpointMgr();

 public:
  static EndpointMgr &GetInstance() {
    static EndpointMgr instance;
    return instance;
  }

  /**
   * Get the set of endpoints.
   * @return Map with <unique device string, Endpoint object>
   */
  const std::map<std::string, std::shared_ptr<Endpoint>> GetEndpoints() const;
};

}  // namespace regnum

#endif  // ENDPOINT_MGR_H_
