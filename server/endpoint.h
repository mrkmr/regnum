// -----------------------------------------------------------------------------
// File: endpoint.h
// -----------------------------------------------------------------------------
//
// Device endpoint wrapper class.

#ifndef ENDPOINT_H_
#define ENDPOINT_H_

#include <map>
#include <mutex>
#include <string>

#include "endpoint_volume.h"
#include "notification_clients.h"
#include "windows-inc.h"

namespace regnum {

// Practically the same as DEVICE_STATE_XXX constants, but has Unknown.
enum class EndpointState {
  ErrorGettingState = 0,
  Active,
  Disabled,
  NotPresent,
  Unknown,
  Unplugged,
};

const std::map<EndpointState, const std::string> EndpointStateToString = {
    {EndpointState::ErrorGettingState, "Error Getting State"},
    {EndpointState::Active, "Active"},
    {EndpointState::Disabled, "Disabled"},
    {EndpointState::NotPresent, "Not Present"},
    {EndpointState::Unknown, "Unknown"},
    {EndpointState::Unplugged, "Unplugged"},
};

// Practically the same as EDataFlow constants, but has Unknown.
enum class EndpointType {
  ErrorGettingType = 0,
  Render,
  Capture,
  All,
  Unknown,
};

const std::map<EndpointType, const std::string> EndpointTypeToString = {
    {EndpointType::ErrorGettingType, "Error Getting Type"},
    {EndpointType::Render, "Render"},
    {EndpointType::Capture, "Capture"},
    {EndpointType::All, "All"},
    {EndpointType::Unknown, "Unknown"},
};

/**
 * Represents a device endpoint.
 */
class Endpoint {
 private:
  CComPtr<IMMDevice> device_ = nullptr;
  CComQIPtr<IAudioEndpointVolume> endpoint_vol_ = nullptr;
  CComQIPtr<IMMEndpoint> endpoint_ = nullptr;

  CComPtr<VolumeNotificationClient> volume_notification_client_ = nullptr;

  // Cache data until we get a notification that it needs updating.
  std::string id_ = "";
  std::string name_ = "";
  std::shared_ptr<EndpointVolume> endpoint_volume_ = nullptr;
  EndpointState state_ = EndpointState::Unknown;
  EndpointType type_ = EndpointType::Unknown;

  const std::string GetIdUncached() const;
  const std::string GetNameUncached() const;
  bool GetMuteUncached() const;
  float GetVolumeUncached() const;
  EndpointState GetStateUncached() const;
  EndpointType GetTypeUncached() const;

 public:
  /*
   * Endpoint Constructor.
   * @param[in] pDevice The device that represents the endpoint.
   */
  Endpoint(CComPtr<IMMDevice> device);

  /**
   * Get unique endpoint ID.
   */
  const std::string GetId() const;

  /**
   * Get the friendly name.
   */
  const std::string GetName() const;

  /**
   * Is this the default one?
   */
  // bool IsDefault();

  /**
   * Is this device muted?
   */
  bool GetMute();

  /**
   * Get volume state.
   */
  float GetVolume();

  /**
   * Get active/disabled string.
   */
  EndpointState GetState() const;

  /**
   * What type of device is this?
   */
  EndpointType GetType() const;
};

}  // namespace regnum

#endif  // ENDPOINT_H_
