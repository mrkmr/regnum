// -----------------------------------------------------------------------------
// File: windows-inc.h
// -----------------------------------------------------------------------------
//
// Windows includes.

#ifndef WINDOWS_INC_H_
#define WINDOWS_INC_H_

// Exclude rarely-used stuff from Windows headers
#define NOGDI
#define WIN32_LEAN_AND_MEAN

#include <atlbase.h>
#include <comdef.h>
#include <endpointvolume.h>
#include <mmdeviceapi.h>
#include <windows.h>

// After mmdeviceapi.h.
#include <Functiondiscoverykeys_devpkey.h>

#endif  // WINDOWS_INC_H_
