// -----------------------------------------------------------------------------
// File: util.h
// -----------------------------------------------------------------------------
//
// Various utility functions.

#ifndef UTIL_H_
#define UTIL_H_

#include <iostream>
#include <map>
#include <stdexcept>
#include <string>

#include "windows-inc.h"

#define GLOG_NO_ABBREVIATED_SEVERITIES
#include <glog/logging.h>

using std::endl;

namespace util {
// TODO: Add a wstring to string cache.
// namespace priv {
//  std::map
//} // priv

/** Throw a message which will exit the program. */
void Fatal(std::string message, HRESULT res);

/** Convert a wide unicode string to a non-wide utf8 string. */
const std::string WideStringToString(const std::wstring& wstr);

/** Convert a non-wide utf8 string to a wide unicode string. */
const wchar_t* StringToWideString(const std::string& str);

}  // namespace util

#endif  // UTIL_H_
