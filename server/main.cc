// -----------------------------------------------------------------------------
// File: main.cc
// -----------------------------------------------------------------------------
//
// ZeroMQ server.

#include <cassert>
#include <chrono>
#include <thread>

#include "glog/logging.h"
#include "util.h"
#include "zmq.h"

int main(int argc, char **argv) {
  HRESULT res = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
  if (res != S_OK) {
    util::Fatal("Failed to CoInitialzeEx()", res);
  }

  FLAGS_logtostderr = 1;
  google::InitGoogleLogging(argv[0]);

  // regnum::EndpointMgr::GetInstance();
  // EndpointMgr endp_mgr = EndpointMgr::getInstance();
  // for (const auto& [id, endp] : endp_mgr.GetEndpoints()) {
  //  LOG(INFO) << id << endl;
  //  LOG(INFO) << "   * name - " << endp.GetName() << endl;
  //  LOG(INFO) << "   * state - " << EndpointStateToString.at(endp.GetState())
  //             << endl;
  //  LOG(INFO) << "   * type - " << EndpointTypeToString.at(endp.GetType())
  //             << endl;

  //  if (endp.IsMuted()) {
  //    LOG(INFO) << "   * mute state: TRUE" << endl;
  //  } else {
  //    LOG(INFO) << "   * mute state: FALSE" << endl;
  //  }

  //  LOG(INFO) << "   * volume level: " << endp.GetVolume() << endl;

  //  LOG(INFO) << "   * ?default?" << endl;
  //}

  // LOG(INFO) << std::flush;
  //
  LOG(INFO) << "Running server." << endl;

  //  Socket to talk to clients
  void *context = zmq_ctx_new();
  void *responder = zmq_socket(context, ZMQ_REP);
  int rc = zmq_bind(responder, "tcp://*:5555");
  assert(rc == 0);

  while (1) {
    char buffer[10];
    zmq_recv(responder, buffer, 10, 0);
    printf("Received Hello\n");
    std::this_thread::sleep_for(std::chrono::milliseconds(200));
    zmq_send(responder, "World", 5, 0);
  }
  return 0;

  // regnum::RunServer();

  return EXIT_SUCCESS;
}
