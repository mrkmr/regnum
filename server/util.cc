// -----------------------------------------------------------------------------
// File: util.cc
// -----------------------------------------------------------------------------

#include "util.h"

#include <assert.h>

#include <iostream>
#include <locale>
#include <sstream>

#include "windows-inc.h"

#define EMPTY_STRING ""

namespace util {

// Throw a message which will exit the program.
void Fatal(std::string message, HRESULT res) {
  LOG(ERROR) << message << endl;

  _com_error err(res);
  LPCTSTR err_msg = err.ErrorMessage();
  LOG(FATAL) << "COM Error Message: " << err_msg << std::endl;

  // LOG(FATAL) should kill us before we get here.
  throw std::runtime_error("Runtime error: check logs.");
}

// Convert a wide Unicode string to an UTF8 string
const std::string util::WideStringToString(const std::wstring& wstr) {
  if (wstr.empty()) {
    return EMPTY_STRING;
  }
  int str_size = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(),
                                     NULL, 0, NULL, NULL);
  assert(str_size != NULL);

  std::string ret(str_size, 0);
  int converted = WideCharToMultiByte(CP_UTF8, 0, &wstr[0], (int)wstr.size(),
                                      &ret[0], str_size, NULL, NULL);
  assert(str_size == converted);
  return ret;
}

// Convert a UTF8 string to a wide Unicode string.
const wchar_t* util::StringToWideString(const std::string& str) {
  wchar_t* ret = NULL;
  if (str.empty()) {
    return L"";
  }
  int str_size =
      MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), NULL, 0);
  assert(str_size != NULL);

  ret = (wchar_t*)malloc(sizeof(wchar_t) * str_size);
  assert(ret != NULL);
  int converted =
      MultiByteToWideChar(CP_UTF8, 0, &str[0], (int)str.size(), ret, str_size);
  assert(str_size == converted);
  return ret;
}

}  // namespace util
