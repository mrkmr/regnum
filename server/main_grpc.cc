// -----------------------------------------------------------------------------
// File: main.cc
// -----------------------------------------------------------------------------
//
// gRPC server.

#include <memory>

#include "endpoint.h"
#include "endpoint_mgr.h"
#include "glog/logging.h"
#include "google/protobuf/repeated_field.h"
#include "grpcpp/ext/proto_server_reflection_plugin.h"
#include "grpcpp/grpcpp.h"
#include "protos/endpoints.grpc.pb.h"
#include "util.h"

namespace regnum {

using google::protobuf::RepeatedPtrField;
using grpc::Channel;
using grpc::Server;
using grpc::ServerBuilder;
using grpc::ServerContext;
using grpc::Status;

// Logic and data behind the server's behavior.
class EndpointsServiceImpl final : public proto::Endpoints::Service {
  Status GetEndpoints(ServerContext* context,
                      const proto::GetEndpointsRequest* request,
                      proto::GetEndpointsResponse* response) {
    EndpointMgr endp_mgr = EndpointMgr::GetInstance();
    for (const auto& [id, endp] : endp_mgr.GetEndpoints()) {
      response->add_endpoint_id(id);
    }

    return Status::OK;
  }

  Status Get(ServerContext* context, const proto::GetRequest* request,
             proto::GetResponse* response) {
    EndpointMgr endp_mgr = EndpointMgr::GetInstance();
    std::string endpoint_id = request->endpoint_id();

    std::shared_ptr<Endpoint> endpoint =
        endp_mgr.GetEndpoints().at(endpoint_id);

    std::string name = endpoint->GetName();

    response->set_name(endpoint->GetName());
    response->set_is_muted(endpoint->GetMute());
    response->set_volume(endpoint->GetVolume());
    response->set_state(static_cast<proto::State>(endpoint->GetState()));
    response->set_type(static_cast<proto::Type>(endpoint->GetType()));

    return Status::OK;
  }
};

void RunServer() {
  std::string server_address("0.0.0.0:50051");
  EndpointsServiceImpl service;

  grpc::EnableDefaultHealthCheckService(true);
  grpc::reflection::InitProtoReflectionServerBuilderPlugin();
  ServerBuilder builder;
  // Listen on the given address without any authentication mechanism.
  builder.AddListeningPort(server_address, grpc::InsecureServerCredentials());
  // Register "service" as the instance through which we'll communicate with
  // clients. In this case it corresponds to an *synchronous* service.
  builder.RegisterService(&service);
  // Finally assemble the server.
  std::unique_ptr<Server> server(builder.BuildAndStart());
  std::cout << "Server listening on " << server_address << std::endl;

  // Wait for the server to shutdown. Note that some other thread must be
  // responsible for shutting down the server for this call to ever return.
  server->Wait();
}

}  // namespace regnum

int main(int argc, char** argv) {
  HRESULT res = CoInitializeEx(nullptr, COINIT_MULTITHREADED);
  if (res != S_OK) {
    util::Fatal("Failed to CoInitialzeEx()", res);
  }

  FLAGS_logtostderr = 1;
  google::InitGoogleLogging(argv[0]);

  // Initialize.
  regnum::EndpointMgr::GetInstance();

  LOG(INFO) << "Running server." << endl;

  regnum::RunServer();

  return EXIT_SUCCESS;
}
