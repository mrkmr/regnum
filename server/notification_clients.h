// -----------------------------------------------------------------------------
// File: notification_clients.h
// -----------------------------------------------------------------------------
//
// Notification client classes for windows endpoints and volume notifications.

#ifndef NOTIFICATION_CLIENTS_H_
#define NOTIFICATION_CLIENTS_H_

#include "endpoint_volume.h"
#include "util.h"
#include "windows-inc.h"

namespace regnum {

// Notification client for all device events.
// https://docs.microsoft.com/en-us/windows/win32/coreaudio/device-events
class EndpointNotificationClient : public IMMNotificationClient {
 private:
  LONG ref_;

 public:
  EndpointNotificationClient() : ref_(1) {}

  ULONG AddRef();
  ULONG Release();
  HRESULT QueryInterface(REFIID riid, void **_interface);

  HRESULT OnDefaultDeviceChanged(EDataFlow flow, ERole role,
                                 LPCWSTR pwstrDeviceId);
  HRESULT OnDeviceAdded(LPCWSTR pwstrDeviceId);
  HRESULT OnDeviceRemoved(LPCWSTR pwstrDeviceId);
  HRESULT OnDeviceStateChanged(LPCWSTR pwstrDeviceId, DWORD dwNewState);
  HRESULT OnPropertyValueChanged(LPCWSTR pwstrDeviceId, const PROPERTYKEY key);
};

// Notification client for volume events (one per device).
// https://docs.microsoft.com/en-us/windows/win32/coreaudio/endpoint-volume-controls
class VolumeNotificationClient : public IAudioEndpointVolumeCallback {
 private:
  LONG ref_;
  std::shared_ptr<EndpointVolume> endpoint_volume_;

 public:
  VolumeNotificationClient(std::shared_ptr<EndpointVolume> endpoint_volume)
      : ref_(1), endpoint_volume_(endpoint_volume) {}

  ULONG AddRef();
  ULONG Release();
  HRESULT QueryInterface(REFIID iid, void **_interface);

  HRESULT OnNotify(PAUDIO_VOLUME_NOTIFICATION_DATA notify);
};

}  // namespace regnum

#endif  // NOTIFICATION_CLIENTS_H_
