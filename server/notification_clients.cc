// -----------------------------------------------------------------------------
// File: notification_clients.cc
// -----------------------------------------------------------------------------

#include "notification_clients.h"

#include "util.h"
#include "windows-inc.h"

namespace regnum {

ULONG EndpointNotificationClient::AddRef() {
  return InterlockedIncrement(&ref_);
}

ULONG EndpointNotificationClient::Release() {
  ULONG ref = InterlockedDecrement(&ref_);
  if (0 == ref) {
    delete this;
  }
  return ref;
}

HRESULT EndpointNotificationClient::QueryInterface(REFIID riid,
                                                   void **_interface) {
  if (IID_IUnknown == riid) {
    AddRef();
    *_interface = (IUnknown *)this;
  } else if (__uuidof(IMMNotificationClient) == riid) {
    AddRef();
    *_interface = (IMMNotificationClient *)this;
  } else {
    *_interface = nullptr;
    return E_NOINTERFACE;
  }
  return S_OK;
}

HRESULT EndpointNotificationClient::OnDefaultDeviceChanged(
    EDataFlow flow, ERole role, LPCWSTR pwstrDeviceId) {
  char *pszFlow = "?????";
  char *pszRole = "?????";

  //_PrintDeviceName(pwstrDeviceId);

  switch (flow) {
    case eRender:
      pszFlow = "eRender";
      break;
    case eCapture:
      pszFlow = "eCapture";
      break;
  }
  switch (role) {
    case eConsole:
      pszRole = "eConsole";
      break;
    case eMultimedia:
      pszRole = "eMultimedia";
      break;
    case eCommunications:
      pszRole = "eCommunications";
      break;
  }

  printf("  -->New default device: flow = %s, role = %s\n", pszFlow, pszRole);
  return S_OK;
}

HRESULT EndpointNotificationClient::OnDeviceAdded(LPCWSTR pwstrDeviceId) {
  //_PrintDeviceName(pwstrDeviceId);

  printf("  -->Added device\n");
  return S_OK;
}

HRESULT EndpointNotificationClient::OnDeviceRemoved(LPCWSTR pwstrDeviceId) {
  //_PrintDeviceName(pwstrDeviceId);

  printf("  -->Removed device\n");
  return S_OK;
}

HRESULT EndpointNotificationClient::OnDeviceStateChanged(LPCWSTR pwstrDeviceId,
                                                         DWORD dwNewState) {
  char *pszState = "?????";

  //_PrintDeviceName(pwstrDeviceId);

  switch (dwNewState) {
    case DEVICE_STATE_ACTIVE:
      pszState = "ACTIVE";
      break;
    case DEVICE_STATE_DISABLED:
      pszState = "DISABLED";
      break;
    case DEVICE_STATE_NOTPRESENT:
      pszState = "NOTPRESENT";
      break;
    case DEVICE_STATE_UNPLUGGED:
      pszState = "UNPLUGGED";
      break;
  }

  printf("  -->New device state is DEVICE_STATE_%s (0x%8.8x)\n", pszState,
         dwNewState);

  return S_OK;
}

HRESULT EndpointNotificationClient::OnPropertyValueChanged(
    LPCWSTR pwstrDeviceId, const PROPERTYKEY key) {
  //_PrintDeviceName(pwstrDeviceId);

  LOG(INFO) << " ==> Changed device property." << endl;
  std::cout << " ==> Changed device property." << endl;
  printf(
      "  -->Changed device property "
      "{%8.8x-%4.4x-%4.4x-%2.2x%2.2x-%2.2x%2.2x%2.2x%2.2x%2.2x%2.2x}#%d\n",
      key.fmtid.Data1, key.fmtid.Data2, key.fmtid.Data3, key.fmtid.Data4[0],
      key.fmtid.Data4[1], key.fmtid.Data4[2], key.fmtid.Data4[3],
      key.fmtid.Data4[4], key.fmtid.Data4[5], key.fmtid.Data4[6],
      key.fmtid.Data4[7], key.pid);
  return S_OK;
}

ULONG VolumeNotificationClient::AddRef() { return InterlockedIncrement(&ref_); }

ULONG VolumeNotificationClient::Release() {
  ULONG ref = InterlockedDecrement(&ref_);
  if (ref == 0) {
    delete this;
  }
  return ref;
}

HRESULT VolumeNotificationClient::QueryInterface(REFIID iid,
                                                 void **_interface) {
  if (iid == IID_IUnknown) {
    AddRef();
    *_interface = (IUnknown *)this;
  } else if (iid == __uuidof(IAudioEndpointVolumeCallback)) {
    AddRef();
    *_interface = (IAudioEndpointVolumeCallback *)this;
  } else {
    *_interface = nullptr;
    return E_NOINTERFACE;
  }
  return S_OK;
}

HRESULT VolumeNotificationClient::OnNotify(
    PAUDIO_VOLUME_NOTIFICATION_DATA notify) {
  if (notify == NULL) {
    return E_INVALIDARG;
  }

  endpoint_volume_->SetMute(notify->bMuted);
  endpoint_volume_->SetVolume(notify->fMasterVolume);

  return S_OK;
}

}  // namespace regnum
